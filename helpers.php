<?php

/** PATH */

/**
 *  Добавляет в начало полную ссылку на сайт
 */
function site_url($path = '')
{
    $SERVER_NAME = SERVER_NAME;
    if ($path == '')
        return $SERVER_NAME;
    if ($path[0] == '/')
        $path = substr($path, 1);

    return $SERVER_NAME . $path;
}

/**
 *  Добавляем полный путь к файлу
 */
function base_path($path = '')
{
    if ($path == '')
        return BASEPATH;
    if ($path[0] == '/' || $path[0] == '\\')
        $path = substr($path, 1);

    $path = BASEPATH  . '/' . $path;
    $path = str_replace('\\', '/', $path);
    $pattern = '/\/{2,}/i'; // удалить лишние // ...
    $path = preg_replace($pattern, '/', $path);

    return $path;
}

/**
 * DEBUG
 * вывод данных в удобочитаемом виде
 * $data - данные для отображения
 * $die - выход из скрипта после выполнения функции
 */
function dump($data, bool $die = false)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';

    if ( $die ) die();
}

/** ARRAY/OBJECT */

/**
 * Выборка данных из массива
 * $items - массив или объект
 * $func - анонимная функция
 *
 * Как пользоваться:
 * $emails = map($users, function($user) {
 * 		return $user['email'];	 
 * }); 
 */
function map($items, $func)
{
	$results = [];
	
	foreach ( $items as $item ) {
		$results[] = $func($item);
	}

	return $results;
}

/**
 * Фильтрация масива (оставить данные по условию)
 * $items - массив или объект
 * $func - анонимная функция
 *
 * Как пользоваться:
 * $emails = filter($users, function($user) {
 * 		return $user['banned']; // если $user['banned'] == true
 * });
 */
function filter($items, $func)
{
    $results = [];

    foreach ( $items as $item ) {
        if ( $func($item) ) {
            $results[] = $item;
        }
    }

    return $results;
}

/**
 * Фильтрация масива (отсечь данные по условию)
 * $items - массив или объект
 * $func - анонимная функция
 *
 * Как пользоваться:
 * $emails = reject($users, function($user) {
 * 		return $user['banned']; // если $user['banned'] == false
 * });
 */
function reject($items, $func)
{
    $results = [];

    foreach ( $items as $item ) {
        if ( !$func($item) ) {
            $results[] = $item;
        }
    }

    return $results;
}

/** DATE/TIME */
// высчитываем разницу между датами
function diffDate($start, $end = null)
{
    $date = new DateTime($start);
    if ( !$end ) $end = new DateTime();
    else $end = new DateTime($end);

    return $date->diff($end);
}

/** OTHER */
// работа с суфиксом
function declOfNum($val, array $suffix)
{
    $mod = $val % 10;

    if ( $mod == 0 || $val == 0 ) {
        return $suffix[2];
    }

    if ( $mod == 1 && $val != 11 ) {
        return $suffix[0];
    }

    if ( $mod >= 2 && $mod <= 4 && ($val < 12 || $val > 14) ) {
        return $suffix[1];
    }

    if ( $mod >= 5 && $mod <= 9 || ($val >= 5 && $val <= 20) ) {
        return $suffix[2];
    }
}

